#!/bin/sh

################################################################################
#
# 2019-12-12 -- jontow@unc.edu
#
# Display running jobs and their elapsed time in a refreshing way, like a
# 1970s Bubble Up soda commercial: https://www.youtube.com/watch?v=EmqJ_EzwAp4
#
# * Set/override OLDERTHAN variable to filter out old/finished jobs
# * Set/override MAXJOBS as needed, but a value too large is a choking hazard
# * Too short of an interval (or too slow of a machine) will cause blank
#   screens and/or delays
# * Passing 'noclear' option as $2 is fun for troubleshooting reasons
#
################################################################################

if [ ! -e ./libawxapi.sh ]; then
    echo "ERROR: missing libawxapi.sh" >&2
    exit 2
fi

# shellcheck disable=SC1091
. ./libawxapi.sh

if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    echo "Syntax: jobtop.sh [interval] [noclear]"
    exit 1
fi

MAXJOBS="${MAXJOBS:-50}"
OLDERTHAN="${OLDERTHAN:-3600}"
INTERVAL="${1:-5}"

jobtmp=$(mktemp list_unified_jobs.XXXXX)
# shellcheck disable=SC2064
trap "rm -f ${jobtmp} ; exit 0" INT QUIT

while true; do
    list_unified_jobs "${MAXJOBS}" | \
        jq -r '.results[] | (.id | tostring) + "," + .status + "," + .name + "," + (.elapsed | tostring) + "," + .finished' \
        >"${jobtmp}"

    if [ "$2" != "noclear" ]; then
        clear
    fi

    printf "%-8s %-12s %-32s %-12s %-32s\n" ID STATUS JOBNAME ELAPSED FINISHED
    echo "-----------------------------------------------------------------------------------------------"

    while read -r job; do
        job_status=$(echo "${job}" | awk -F, '{print $2}')
        case "${job_status}" in
            "new"|"pending"|"waiting"|"running")
                echo "${job}" | awk -F, '{printf "%-8s %-12s %-32s %-12s %-32s\n", $1, $2, $3, $4, $5}'
                ;;
            "successful"|"failed"|"error"|"canceled")
                finished=$(echo "${job}" | awk -F, '{print $5}')
                finished_epoch=$(date --date="${finished}" +"%s")
                now=$(date +"%s")
                timediff=$((now - finished_epoch))
                if [ "${timediff}" -le "${OLDERTHAN}" ]; then
                    echo "${job}" | awk -F, '{printf "%-8s %-12s %-32s %-12s %-32s\n", $1, $2, $3, $4, $5}'
                fi
                ;;
            *)
                echo "unknown job line: ${job}"
                ;;
        esac
    done < "${jobtmp}"
    rm "${jobtmp}"

    sleep "${INTERVAL}"
done
