#!/bin/sh

################################################################################
#
# 2019-12-11 -- jontow@unc.edu
#
# Do not run directly: only a library file!
# To get some usage examples, try: "grep FUNCTION libawxapi.sh"
#
# Example Library Usage:
#
#   . ./libawxapi.sh
#   cd /path/to/gitrepo
#   init_git_env
#   echo ${job_name}
#   project_id=$(create_project)
#   wait_on_job_end ${job_name} project_update
#   template_id=$(create_template ${project_id})
#   job_id=$(create_job ${template_id})
#   wait_on_job_end ${job_name} job
#   delete_template ${template_id}
#   delete_project ${project_id}
#
################################################################################

# Load config
if [ -e config.sh ]; then
    # shellcheck disable=SC1091
    . ./config.sh
else
    echo "ERROR: cannot load config.sh" >&2
    return 1
fi

# Test config validity: only test for required external
# parameters.  Everything else should have a valid default
# in this file.
if  [ -z "${AWX_HOST}"   ] ||
    [ -z "${AUTH_TOKEN}" ]; then
    echo "ERROR: API not properly configured." >&2
    return 1
fi

# http or https
URL_PROTO="${URL_PROTO:-http}"

# Sleep between retries (seconds)
SLEEP_INTERVAL="${SLEEP_INTERVAL:-1}"

# This number of retries constitutes a timeout (seconds)
JOB_TIMEOUT="${JOB_TIMEOUT:-30}"

# Wait this long before deleting temporary resources (seconds)
WAIT_BEFORE_DELETE="${WAIT_BEFORE_DELETE:-30}"

# Collision avoidance prefix string
PREFIX_STR="${PREFIX_STR:-AWXTEST}"

# Debugging flag, non-empty to enable.
DEBUG="${DEBUG:-}"

# curl debugging flags, override with "-v"
CURL_DEBUG="${CURL_DEBUG:--s}"

################################################################################
# FUNCTION: init_git_env
# Initialize various GIT_ variables required to run other functions.
#
# No parameters.
#
# Returns (prints to stdout): nothing
#
init_git_env()
{
    if [ ! -z "${DEBUG}" ]; then
        echo "DEBUG: entering init_git_env()" >&2
    fi
    GIT_ORIGIN="${GIT_ORIGIN:-origin}"
    GIT_REMOTE="${GIT_REMOTE:-$(git remote get-url "${GIT_ORIGIN}")}"
    GIT_BRANCH="$(git branch | awk '/^*/ {print $2}')"

    if  [ -z "${GIT_ORIGIN}" ] ||
        [ -z "${GIT_REMOTE}" ] ||
        [ -z "${GIT_BRANCH}" ]; then
        echo "ERROR: init_git_env() empty GIT_ variables." >&2
        return 2
    fi

    export GIT_ORIGIN GIT_REMOTE GIT_BRANCH
}

################################################################################
# FUNCTION: list_unified_jobs [number_of_jobs]
# Retrieve unified jobs list as a blob of json.
#
# Optional parameters:
# $1 = number of jobs to return (default: 20)
# $2 = order by (default: -started)
#
# Returns (prints to stdout): json string
#
# shellcheck disable=SC2120
list_unified_jobs()
{
    if [ ! -z "${DEBUG}" ]; then
        echo "DEBUG: entering list_unified_jobs()" >&2
    fi

    page_size="${1:-20}"
    order_by="${2:--started}"
    curl "${CURL_DEBUG}" -X GET \
        "${URL_PROTO}://${AWX_HOST}/api/v2/unified_jobs/?page_size=${page_size}&order_by=${order_by}&not__launch_type=sync" \
        -H 'Accept: */*' \
        -H 'Accept-Encoding: gzip, deflate' \
        -H "Authorization: ${AUTH_TOKEN}" \
        -H 'Cache-Control: no-cache' \
        -H 'Connection: keep-alive' \
        -H "Host: ${AWX_HOST}" \
        -H 'cache-control: no-cache'
}

################################################################################
# FUNCTION: list_inventories [number-of-inventories]
# Retrieve list of inventories as a blob of json
#
# Optional parameters:
# $1 = number of jobs to return (default: 20)
#
# Returns (prints to stdout): json string
#
# shellcheck disable=SC2120
list_inventories()
{
    if [ ! -z "${DEBUG}" ]; then
        echo "DEBUG: entering list_inventories()" >&2
    fi

    page_size=${1:-20}
    curl "${CURL_DEBUG}" -X GET \
        "${URL_PROTO}://${AWX_HOST}/api/v2/inventories/?page_size=${page_size}&order_by=name" \
        -H 'Accept: */*' \
        -H 'Accept-Encoding: gzip, deflate' \
        -H "Authorization: ${AUTH_TOKEN}" \
        -H 'Cache-Control: no-cache' \
        -H 'Connection: keep-alive' \
        -H "Host: ${AWX_HOST}" \
        -H 'cache-control: no-cache'
}

################################################################################
# FUNCTION: get_inventory_id <inventory_name>
# Obtain numeric inventory id when provided an inventory name
#
# Required parameters:
# $1 = inventory name
#
get_inventory_id()
{
    inv_name="$1"
    list_inventories 50 | jq -r ".results[] | select(.name == \"${inv_name}\") | .id"
}

################################################################################
# FUNCTION: generate_project_name
# Generate a project name with a known prefix to avoid name collisions
#
# No parameters.
#
# Returns (prints to stdout): single line string
#
generate_project_name()
{
    if [ ! -z "${DEBUG}" ]; then
        echo "DEBUG: entering generate_project_name()" >&2
    fi

    echo "${PREFIX_STR}__${GIT_BRANCH}"
}

################################################################################
# FUNCTION: create_project [project_name]
# Create an AWX project for a git remote
#
# Optional parameters:
# $1 = project name (defaults to a generated value)
#
# Returns (prints to stdout): project id as integer
#
create_project()
{
    if [ ! -z "${DEBUG}" ]; then
        echo "DEBUG: entering create_project()" >&2
    fi

    project_name="${1:-$(generate_project_name)}"
    if [ -z "${project_name}" ]; then
        echo "ERROR: create_project() missing project name" >&2
        return 2
    fi

    project_out=$(curl "${CURL_DEBUG}" -X POST \
        "${URL_PROTO}://${AWX_HOST}/api/v2/projects/" \
        -H 'Accept: */*' \
        -H 'Accept-Encoding: gzip, deflate' \
        -H "Authorization: ${AUTH_TOKEN}" \
        -H 'Cache-Control: no-cache' \
        -H 'Connection: keep-alive' \
        -H 'Content-Type: application/json' \
        -H "Host: ${AWX_HOST}" \
        -H 'cache-control: no-cache' \
        -d "{
            \"name\":\"${project_name}\",
            \"organization\":1,
            \"scm_type\":\"git\",
            \"base_dir\":\"/var/lib/awx/projects\",
            \"scm_url\":\"${GIT_REMOTE}\",
            \"scm_branch\":\"${GIT_BRANCH}\",
            \"scm_clean\":true,
            \"scm_delete_on_update\":true,
            \"scm_update_on_launch\":true,
            \"scm_update_cache_timeout\":\"60\",
            \"custom_virtualenv\":null
        }")
    project_id=$(echo "${project_out}" | tr '\r\n' ' ' | jq .id)
    if [ ! -z "${DEBUG}" ]; then
        echo "project out: ${project_out}" >&2
    fi

    if ! echo "${project_id}" | grep -q -E '[0-9]+'; then
        echo "ERROR: project creation failed" >&2
        return 2
    fi

    echo "${project_id}"
}

################################################################################
# FUNCTION: create_template <project_id> [inventory_name] [ansible_job_type] [ansible_playbook]
# Create AWX job template
#
# Required Parameters:
# $1 = project id
#
# Optional Parameters:
# $2 = inventory_name: defaults to the inventory resolving as id '1'
# $3 = ansible_job_type (run, check): defaults to 'run'
# $4 = ansible_playbook: defaults to 'playbook.yml'
#
# Returns (prints to stdout): template id as integer
#
create_template()
{
    if [ ! -z "${DEBUG}" ]; then
        echo "DEBUG: entering create_template()" >&2
    fi

    project_id="$1"
    inventory_name="${2:-${inventory}}"
    inventory_id=$(get_inventory_id "${inventory_name}")
    ansible_job_type="${3:-run}"
    ansible_playbook="${4:-playbook.yml}"

    if  [ -z "${project_id}" ] || \
        [ -z "${ansible_job_type}" ] || \
        [ -z "${ansible_playbook}" ]; then
        echo "ERROR: create_template() missing parameters" >&2
        return 2
    fi

    template_out=$(curl "${CURL_DEBUG}" -X POST \
        "${URL_PROTO}://${AWX_HOST}/api/v2/job_templates/" \
        -H 'Accept: */*' \
        -H 'Accept-Encoding: gzip, deflate' \
        -H "Authorization: ${AUTH_TOKEN}" \
        -H 'Cache-Control: no-cache' \
        -H 'Connection: keep-alive' \
        -H 'Content-Type: application/json' \
        -H "Host: ${AWX_HOST}" \
        -H 'cache-control: no-cache' \
        -d "{
            \"name\":\"${PREFIX_STR}__${GIT_BRANCH}\",
            \"job_type\":\"${ansible_job_type}\",
            \"inventory\":${inventory_id},
            \"project\":${project_id},
            \"playbook\":\"${ansible_playbook}\",
            \"verbosity\":0,
            \"job_tags\":\"\",
            \"skip_tags\":\"\",
            \"custom_virtualenv\":null,
            \"job_slice_count\":1,
            \"timeout\":0,
            \"diff_mode\":true,
            \"become_enabled\":true,
            \"allow_callbacks\":false,
            \"enable_webhook\":false,
            \"webhook_credential\":null,
            \"forks\":0,
            \"ask_diff_mode_on_launch\":false,
            \"ask_scm_branch_on_launch\":false,
            \"ask_tags_on_launch\":false,
            \"ask_skip_tags_on_launch\":false,
            \"ask_limit_on_launch\":false,
            \"ask_job_type_on_launch\":false,
            \"ask_verbosity_on_launch\":false,
            \"ask_inventory_on_launch\":false,
            \"ask_variables_on_launch\":false,
            \"ask_credential_on_launch\":false,
            \"extra_vars\":\"\",
            \"survey_enabled\":false
        }")
    template_id=$(echo "${template_out}" | tr '\r\n' ' ' | jq .id)
    if [ ! -z "${DEBUG}" ]; then
        echo "template out: ${template_out}" >&2
    fi

    if ! echo "${template_id}" | grep -q -E '[0-9]+'; then
        echo "ERROR: template creation failed" >&2
        return 2
    fi

    echo "${template_id}"
}

################################################################################
# FUNCTION: create_job <template_id>
# Create a job from a given template
#
# Required Parameters:
# $1 = template id
#
# Returns (prints to stdout): job id as integer
#
create_job()
{
    if [ ! -z "${DEBUG}" ]; then
        echo "DEBUG: entering create_job()" >&2
    fi

    template_id="$1"

    if  [ -z "${template_id}" ]; then
        echo "ERROR: create_job() missing parameters" >&2
        return 2
    fi

    job_out=$(curl "${CURL_DEBUG}" -X POST \
        "${URL_PROTO}://${AWX_HOST}/api/v2/job_templates/${template_id}/launch/" \
        -H 'Accept: */*' \
        -H 'Accept-Encoding: gzip, deflate' \
        -H "Authorization: ${AUTH_TOKEN}" \
        -H 'Cache-Control: no-cache' \
        -H 'Connection: keep-alive' \
        -H "Host: ${AWX_HOST}" \
        -H 'cache-control: no-cache')
    job_id=$(echo "${job_out}" | tr '\r\n' ' ' | jq .id)
    if [ ! -z "${DEBUG}" ]; then
        echo "job out: ${job_out}" >&2
    fi

    if ! echo "${job_id}" | grep -q -E '[0-9]+'; then
        echo "ERROR: job creation failed" >&2
        return 2
    fi

    echo "${job_id}"
}

################################################################################
# FUNCTION: wait_on_job_end [job_name] [job_type]
# Wait for a job to end, report completion status
#
# Optional Parameters:
# $1 = job name: defaults to a generated value
# $2 = job type (job, project_update, ......): defaults to 'job'
#
# Returns (prints to stdout): job status as string
#
wait_on_job_end()
{
    if [ ! -z "${DEBUG}" ]; then
        echo "DEBUG: entering wait_on_job_end()" >&2
    fi

    job_name="${1:-${PREFIX_STR}__${GIT_BRANCH}}"
    awx_job_type="${2:-job}"

    if [ -z "${job_name}" ] || [ -z "${awx_job_type}" ]; then
        echo "ERROR: wait_on_job_end() missing parameters" >&2
        return 2
    fi

    waiting_on_job_end=
    loop_count=0
    job_end_json=$(mktemp job_end_json.XXXXX)
    while [ -z "${waiting_on_job_end}" ] && \
          [ "${loop_count}" -lt "${JOB_TIMEOUT}" ]; do
        # Sleep at the top of the loop to give better chance of first-run success
        sleep "${SLEEP_INTERVAL}"
        # shellcheck disable=SC2119
        list_unified_jobs >"${job_end_json}"
        if [ ! -z "${DEBUG}" ]; then
            echo "job_end_json: ${job_end_json}" >&2
        fi

        ### Get job status
        # shellcheck disable=SC2086
        #echo jq -r ".results[] | select(.summary_fields.unified_job_template.unified_job_type == \"${awx_job_type}\" and .summary_fields.unified_job_template.name == \"${PREFIX_STR}__${GIT_BRANCH}\").status" "${job_end_json}"
        ujobs_status=$(jq -r ".results[] | select(.summary_fields.unified_job_template.unified_job_type == \"${awx_job_type}\" and .summary_fields.unified_job_template.name == \"${PREFIX_STR}__${GIT_BRANCH}\").status" "${job_end_json}")
        #echo "DEEEEBUG: ${ujobs_status}"
        if [ ! -z "${ujobs_status}" ]; then
            case "${ujobs_status}" in
              "successful"|"failed"|"error"|"canceled")
                if [ ! -z "${DEBUG}" ] || [ ! -z "${VERBOSE}" ]; then
                    echo "Job completed with status: ${ujobs_status}" >&2
                fi
                ujobs_id=$(jq ".results[] | select(.summary_fields.unified_job_template.unified_job_type == \"${awx_job_type}\" and .summary_fields.unified_job_template.name == \"${PREFIX_STR}__${GIT_BRANCH}\" and .status == \"successful\") | .id" "${job_end_json}")
                waiting_on_job_end="no"
                break
                ;;
              *)
                echo "WAITING: Job not completed, current status: ${ujobs_status}" >&2
                ;;
            esac
        fi

        loop_count=$((loop_count + 1))
    done
    if [ ! -z "${ujobs_id}" ]; then
        if [ ! -z "${DEBUG}" ] || [ ! -z "${VERBOSE}" ]; then
            echo "OK: SCM Update job completed successfully with job id: ${ujobs_id}" >&2
        fi
    fi

    rm "${job_end_json}"

    echo "${ujobs_status}"
}

################################################################################
# FUNCTION: delete_template <template_id>
#
# Required parameters:
# $1 = template id
#
# Returns (prints to stdout): nothing
#
delete_template()
{
    if [ ! -z "${DEBUG}" ]; then
        echo "DEBUG: entering delete_template()" >&2
    fi

    template_id="$1"
    if [ -z "${template_id}" ]; then
        echo "ERROR: delete_template() missing template name" >&2
        return 2
    fi

    curl "${CURL_DEBUG}" -X DELETE \
        "${URL_PROTO}://${AWX_HOST}/api/v2/job_templates/${template_id}/" \
        -H 'Accept: */*' \
        -H 'Accept-Encoding: gzip, deflate' \
        -H "Authorization: ${AUTH_TOKEN}" \
        -H 'Cache-Control: no-cache' \
        -H 'Connection: keep-alive' \
        -H 'cache-control: no-cache'
}

################################################################################
# FUNCTION: delete_project [project_id]
#
# Required parameters:
# $1 = project id
#
# Returns (prints to stdout): nothing
#
delete_project()
{
    if [ ! -z "${DEBUG}" ]; then
        echo "DEBUG: entering delete_project()" >&2
    fi

    project_id="$1"
    if [ -z "${project_id}" ]; then
        echo "ERROR: delete_project() missing project name" >&2
        return 2
    fi

    curl "${CURL_DEBUG}" -X DELETE \
        "${URL_PROTO}://${AWX_HOST}/api/v2/projects/${project_id}/" \
        -H 'Accept: */*' \
        -H 'Accept-Encoding: gzip, deflate' \
        -H "Authorization: ${AUTH_TOKEN}" \
        -H 'Cache-Control: no-cache' \
        -H 'Connection: keep-alive' \
        -H 'cache-control: no-cache'
}

# EOF
