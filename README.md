Overview
========

Distributed as a library, example configuration, and sample tools.

Dependencies
============

To use these tools, you'll need a standard Linux/BSD based host with a few tools installed:

* curl
* jq

Configuration
=============

```
cp config.sh.example config.sh
vi config.sh
  --> alter values to suit environment
```

Prebuilt Tools
==============

* awxtest.sh: run a complete job 'test cycle', given a repo (create, run, delete)
* jobtop.sh: goofy top(1)-like script that continuously shows AWX job status

Usage, Examples
===============

Various ways to use library/tools, but easiest is if your case fits into one of the sample tools.  A few examples:

Run an ansible 'check' job on a playbook.yml in somerepo:

```
./awxtest.sh check ~/path/to/somerepo
```

List/inspect most recent 45 AWX jobs by status, counted:

```
$ . ./libawxapi.sh
$ list_unified_jobs 45 | jq -r .results[].status | sort | uniq -c
      3 failed
     42 successful
```

Discover functions provided by libawxapi.sh:

```
$ grep FUNCTION libawxapi.sh 
# To get some usage examples, try: "grep FUNCTION libawxapi.sh"
# FUNCTION: init_git_env
# FUNCTION: list_unified_jobs [number_of_jobs]
....
```

List inventories (CSV format: id,name):

```
$ list_inventories | jq '.results[] | (.id | tostring) + "," + .name'
"1,Demo Inventory"
"2,test_inv"
```

Misc Notes/Development
======================

If you're using the functions in this library directly and make changes, you'll likely have to "reload" (". ./libawxapi.sh") before your changes will be accessible.

You can obtain additional verbose/debug output by overriding the values of the following variables:

* VERBOSE: set on CLI or in config.sh to *any non-empty string* for additional output
* DEBUG: set on CLI or in config.sh to *any non-empty string* for even more output
* CURL_DEBUG: set to "-v" for verbose curl output
