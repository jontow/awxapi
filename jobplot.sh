#!/bin/sh

################################################################################
#
# 2020-01-09 -- jontow@unc.edu
#
# Generate an elapsed-time dataset for finished(successful) jobs.
# * Set/override OLDERTHAN variable to filter out old/finished jobs
# * Set/override MAXJOBS as needed, but a value too large is a choking hazard
#
################################################################################

if [ ! -e ./libawxapi.sh ]; then
    echo "ERROR: missing libawxapi.sh" >&2
    exit 2
fi

# shellcheck disable=SC1091
. ./libawxapi.sh

if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    echo "Syntax: jobplot.sh [jobs]"
    exit 1
fi

MAXJOBS="${MAXJOBS:-50}"
OLDERTHAN="${OLDERTHAN:-3600}"

if [ ! -z "$1" ]; then
    MAXJOBS="$1"
fi

rawjobtmp=$(mktemp list_unified_jobs.raw.XXXXX)
jobtmp=$(mktemp list_unified_jobs.XXXXX)
# shellcheck disable=SC2064
trap "rm -f ${jobtmp} ; exit 0" INT QUIT

list_unified_jobs "${MAXJOBS}" "-finished" >"${rawjobtmp}"

jq -r '.results[] | (.id | tostring) + "," + .status + "," + .name + "," + (.elapsed | tostring) + "," + .finished' \
    "${rawjobtmp}" >"${jobtmp}"

while read -r job; do
    job_status=$(echo "${job}" | awk -F, '{print $2}')
    case "${job_status}" in
        # We don't want jobs that haven't finished successfully, so commented:
        #"new"|"pending"|"waiting"|"running")
        #    echo "${job}" | awk -F, '{printf "%s,%s,%s,%s,%s\n", $1, $2, $4, $3, $5}'
        #    ;;
        #"successful"|"failed"|"error"|"canceled")

        "successful")
            finished=$(echo "${job}" | awk -F, '{print $5}')
            finished_epoch=$(date --date="${finished}" +"%s")
            now=$(date +"%s")
            timediff=$((now - finished_epoch))
            if [ "${timediff}" -le "${OLDERTHAN}" ]; then
                #echo "${job}" | awk -F, '{printf "%s,%s,%s,%s,%s\n", $1, $2, $4, $3, $5}'

                # Filter on specific job names
                echo "${job}" | awk -F, '/Linux Master Run/ {print $4}'

                # Print elapsed time for all jobs
                #echo "${job}" | awk -F, '{print $4}'
            fi
            ;;
        *)
            #echo "#ERR: unknown job line: ${job}" >&2
            ;;
    esac
done < "${jobtmp}"

rm "${rawjobtmp}"
rm "${jobtmp}"
