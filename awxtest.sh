#!/bin/sh

################################################################################
#
# 2019-12-11 -- jontow@unc.edu
#
# Examples:
#
# GIT_REMOTE="https://gitlab.com/jontow/fbrepo.git" sh awxtest.sh check
# DEBUG=true GIT_REMOTE="https://gitlab.com/jontow/fbrepo.git" sh awxtest.sh run
#
################################################################################

if [ ! -e ./libawxapi.sh ]; then
    echo "ERROR: missing libawxapi.sh" >&2
    exit 1
fi

# shellcheck disable=SC1091
. ./libawxapi.sh

syntax()
{
    echo "Syntax: awxtest.sh <check|run> <local_repo_directory> [playbook.yml] [inventory_name]"
}

# Required number of parameters supplied?
if [ -z "$2" ]; then
    syntax
    exit 1
fi

job_type="$1"
if  [ "${job_type}" != "check" ] && \
    [ "${job_type}" != "run" ]; then
    syntax
    exit 1
fi

repo_dir="$2"
if [ ! -d "${repo_dir}" ]; then
    echo "ERROR: cannot find local repo dir: ${repo_dir}" >&2
    exit 1
fi

playbook="${playbook:-playbook.yml}"
if [ ! -z "$3" ]; then
    playbook="$3"
fi

inventory="${inventory:-foreman}"
if [ ! -z "$4" ]; then
    inventory="$4"
fi

# Initialize to 0, assuming clean exit unless proven wrong.
retcode=0

# init_git_env needs to be run from a clone of the git repo we're testing, or
# we would have to manually override GIT_* in the environment.
oldpwd=$(pwd)
cd "${repo_dir}" || exit 1
echo "Initializing git environment:"
init_git_env
echo "  GIT_ORIGIN: ${GIT_ORIGIN}"
echo "  GIT_REMOTE: ${GIT_REMOTE}"
echo "  GIT_BRANCH: ${GIT_BRANCH}"
cd "${oldpwd}" || exit 1

project_name="$(generate_project_name)"
echo "Creating project for ${GIT_REMOTE} ... (project name: ${project_name})"
project_id=$(create_project "${project_name}")
if [ -z "${project_id}" ]; then
    # ERROR is printed by library
    exit 2
else
    echo "  OK: project id: ${project_id}"
fi

# To move on, we need to confirm that the 'SCM Update' job was scheduled and
# completed successfully, or our job created later cannot run.  The following
# loop just iterates on the 'unified_jobs' waiting for a successful match.
echo "Waiting on SCM Update job completion for ${project_name}..."
job_status=$(wait_on_job_end "${project_name}" project_update)
echo "Job completed with status: ${job_status}"

# Create a new (TEMPORARY) template
echo "Creating template for project(${project_id}), inventory(${inventory}), job_type(${job_type}), playbook(${playbook})"
template_id=$(create_template "${project_id}" "${inventory}" "${job_type}" "${playbook}")
if [ -z "${template_id}" ]; then
    # ERROR is printed by library
    if [ -z "${DEBUG}" ]; then
        echo "Cleaning up project."
        delete_project "${project_id}"
    fi
    exit 2
else
    echo "  OK: template id: ${template_id}"
fi

# Create a new job
echo "Creating job with template ${template_id} to run against inventory ${inventory}"
job_id=$(create_job "${template_id}" "${inventory}")
if [ -z "${job_id}" ]; then
    # ERROR is printed by library
    if [ -z "${DEBUG}" ]; then
        echo "Cleaning up template."
        delete_project "${template_id}"
        echo "Cleaning up project."
        delete_project "${project_id}"
    fi
    exit 2
else
    echo "  OK: job id: ${job_id}"
fi

# To move on, we need to confirm that our newly created job was scheduled and
# completed successfully, or our test has failed.  The following loop just
# iterates on the 'unified_jobs' waiting for a successful match.
echo "Waiting on final job completion for ${project_name}..."
job_status=$(wait_on_job_end "${project_name}" job)
echo "Job completed with status: ${job_status}"

# Defer exit to allow cleanup, but set return code for eventual end.
case "${job_status}" in
    "successful")
        retcode=0
        ;;
    *)
        retcode=5
        ;;
esac

echo "All known jobs completed: sleeping for ${WAIT_BEFORE_DELETE}sec before deleting jobs to allow for inspection"
echo ""
echo "Link to job output:"
echo ""
echo "  ${URL_PROTO}://${AWX_HOST}/#/jobs/playbook/${job_id}/"
echo ""
sleep "${WAIT_BEFORE_DELETE}"

echo "Deleting template_id ${template_id}"
if [ ! -z "${DEBUG}" ]; then
    echo "DEBUG: not actually deleting template because debugging."
else
    delete_template "${template_id}"
fi

echo "Deleting  project_id ${project_id}"
if [ ! -z "${DEBUG}" ]; then
    echo "DEBUG: not actually deleting project because debugging."
else
    delete_project "${project_id}"
fi

exit "${retcode}"
